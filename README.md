### Инфо
Лишнее от рельс не чистил.

app/controllers<br/>
app/models<br/>
db/migrate

Спеков нет, но по хорошему, стоит написать. По заданию они не требовались.

Дополнительно есть роут для расширенной статистики
/urls/:short_url/stats_long

short_url в урлах использовать без http://server_name/

###Примеры:
curl -X POST -d="" -v "http://localhost:3000/urls/"</br>
Empty url.

curl -X POST -d "url=http://google.com" "http://localhost:3000/urls"</br>
http://localhost:3000/1

curl -X GET "http://localhost:3000/urls/unknown"<br/>
short_url not found

curl -X GET "http://localhost:3000/urls/1"<br/>
http://google.com

curl -X GET "http://localhost:3000/urls/1/stats"<br/>
1

curl -X GET "http://localhost:3000/urls/1/stats_long"<br/>
127.0.0.1 1

### Задание

●	Необходимо написать url shorterer.<br/>

●	Необходимо реализовать сервис сокращения ссылок. Данный сервис должен реализовывать 3 запроса:<br/>
POST /urls который возвращает короткий url<br/>
GET /urls/:short_url который возвращает длинный URL и увеличивает счетчик запросов на 1<br/>
GET /urls/:short_url/stats который возвращает количество переходов по URL<br/>

●	Проект необходимо реализовать на Ruby On Rails.<br/>
База - на выбор.<br/>

●	Необходимо вести статистику по уникальным IP запросов.<br/>

Ответ нужно прислать в виде ссылки на гитхаб

<!--
# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
-->
