class Init < ActiveRecord::Migration[6.1]
  def change
    create_table :urls do |t|
      t.string :long, null: false, index: true
      t.string :short, null: false, index: true
      t.timestamps
    end

    create_table :ip_stats do |t|
      t.belongs_to :urls, null: false, index: true
      t.string :ip, null: false
      t.integer :stats, null: false, default: 0
      t.timestamps
    end
  end
end
