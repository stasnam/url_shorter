Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  # POST /urls который возвращает короткий url
  # GET /urls/:short_url который возвращает длинный URL и увеличивает счетчик запросов на 1
  # GET /urls/:short_url/stats который возвращает количество переходов по URL

  post 'urls', to: 'url#long_to_short'
  get 'urls/:short_url', to: 'url#short_to_long'
  get 'urls/:short_url/stats', to: 'url#stats'
  get 'urls/:short_url/stats_long', to: 'url#stats_long'
end
