def magick(params)
  puts "Params = #{params}"
  [
      {name: "John", surname: "Doe", age: 33},
      {name: "John", surname: "Doe", age: 34},
      {name: "John", surname: "Doe", age: 35}
  ]
end

class Relations
  include Enumerable

  def initialize
    @params = {}
    @order = nil
  end

  def where(params)
    @params.merge!(params)
    self
  end

  def order(params)
    @order = params
    self
  end

  def each(&block)
    magick([@params, @order]).each(&block)
  end
end

class ActiveRecord
  # your code here
  def self.where(params)
    rel = Relations.new
    rel.where(params)
    return rel
  end

  def self.order(params)
    rel = Relations.new
    rel.order(params)
    return rel
  end
end

class User < ActiveRecord
end

class Admin < ActiveRecord
end

ages = User.where(name: "John").where(surname: "Doe").order("age").map do |u|
  u[:age]
end
puts "Ages = #{ages}"

ages = Admin.where(login: "admin").order("created_at").map do |u|
  u[:age]
end
puts "Ages = #{ages}"