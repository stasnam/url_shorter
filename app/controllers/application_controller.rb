class ApplicationController < ActionController::API
  def render_error(message, code=400)
    render plain: message, status: code
  end
end
