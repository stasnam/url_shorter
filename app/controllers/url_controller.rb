class UrlController < ApplicationController
  def _generate_short(_long)
    # TODO: тут какая-то нормальная ф-ция, для генерации урлов
    #   и проверки их уникальности.
    code = Url.maximum('id') || 0
    code += 1
    code
  end

  def long_to_short
    long = params['url']
    unless long
      render_error('Empty url.')
      return
    end
    row = Url.find_by_long(long)
    unless row
      short = _generate_short(long)
      row = Url.create(long: long, short: short)
    end
    full_url = "http://#{request.host_with_port}/#{row.short}"
    render plain: full_url #, content_type: 'text/plain'
  end

  def short_to_long
    short = params['short_url']
    row = Url.find_by_short(short)
    # TODO: Если нужна статистика по ненайденым урлам, то надо переделать БД
    #   (лучше, наверное, в отдельную таблицу их) и поправить тут.
    unless row
      render_error('short_url not found', 404)
      return
    end

    ActiveRecord::Base.transaction do
      stats = IpStats.find_by(urls_id: row.id, ip: request.remote_ip)
      if stats
        stats.lock!
      else
        stats = IpStats.new(urls_id: row.id, ip: request.remote_ip)
      end

      stats.stats += 1
      stats.save!
    end
    render plain: row.long
  end

  def stats
    short = params['short_url']
    url = Url.find_by_short(short)
    unless url
      render plain: 0
      return
    end

    summary = IpStats.where(urls_id: url.id).sum(:stats)
    render plain: summary.to_s
  end

  def stats_long
    # В зависимости от ТЗ можно ошибку кидать. А лучше завернуть в json со статусом ответа и данными.
    # Ибо клиенту может непонятно, '' это нормальный ответ или "что-то пошло не так".
    short = params['short_url']
    url = Url.find_by_short(short)
    unless url
      render_error('', 404)
      return
    end

    stats = IpStats.select('ip, stats').where(urls_id: url.id).order('stats desc')
    result = ''
    stats.each do |st|
      result += "#{st.ip} #{st.stats}\n"
    end
    render plain: result
  end
end
